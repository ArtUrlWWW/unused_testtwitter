package com.example.name.khartn.twitter.test2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import com.example.name.khartn.twitter.test2.threads.TwittsLoader;
import com.example.name.khartn.twitter.test2.utils.Vars;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Vars.act=this;
			
	}
	
	@Override
	protected void onResume() {
		super.onResume();		
		TwittsLoader twittsLoader=new TwittsLoader("khartnjava");
		twittsLoader.start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void sendTweet(View view) {
		Intent intent=new Intent(this, SendTweet.class);
		startActivity(intent);
		
	}

}
