package com.example.name.khartn.twitter.test2;

import com.example.name.khartn.twitter.test2.threads.TwittsLoader;
import com.example.name.khartn.twitter.test2.utils.Vars;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class SecondActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Vars.act=this;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		TwittsLoader twittsLoader=new TwittsLoader(Vars.secondUserName);
		twittsLoader.start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
