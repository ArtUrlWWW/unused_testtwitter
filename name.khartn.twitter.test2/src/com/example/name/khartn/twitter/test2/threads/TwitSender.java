package com.example.name.khartn.twitter.test2.threads;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Calendar;
import java.util.UUID;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import android.util.Log;

import com.example.name.khartn.twitter.test2.utils.Encoder;
import com.example.name.khartn.twitter.test2.utils.Vars;

public class TwitSender extends Thread {

	String messText;

	public TwitSender(String messText) {
		this.messText = messText;
	}

	public void run() {

		try {
			String uuid_string = UUID.randomUUID().toString();
			uuid_string = uuid_string.replaceAll("-", "");
			String oauth_nonce = uuid_string; // any relatively random
												// alphanumeric string will work
												// here

			String oauth_signature_method = "HMAC-SHA1";

			String mesageText = messText;

			// get the timestamp
			Calendar tempcal = Calendar.getInstance();
			long ts = tempcal.getTimeInMillis();// get current time in
												// milliseconds
			String oauth_timestamp = (new Long(ts / 1000)).toString(); // then
																		// divide
																		// by
																		// 1000
																		// to
																		// get
																		// seconds

			// the parameter string must be in alphabetical order, "text"
			// parameter added at end
			String parameter_string = "oauth_consumer_key=" + Vars.twitter_consumer_key + "&oauth_nonce=" + oauth_nonce + "&oauth_signature_method="
					+ oauth_signature_method + "&oauth_timestamp=" + oauth_timestamp + "&oauth_token=" + Encoder.encode(Vars.oauth_token)
					+ "&oauth_version=1.0&status=" + Encoder.encode(mesageText);

			String twitter_endpoint = "https://api.twitter.com/1.1/statuses/update.json";

			String signature_base_string = "POST" + "&" + Encoder.encode(twitter_endpoint) + "&" + Encoder.encode(parameter_string);
			String oauth_signature = "";
			try {
				oauth_signature = Encoder.computeSignature(signature_base_string, Vars.twitter_consumer_secret + "&" + Encoder.encode(Vars.oauth_token_secret));
			} catch (GeneralSecurityException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			String authorization_header_string = "OAuth oauth_consumer_key=\"" + Vars.twitter_consumer_key
					+ "\",oauth_signature_method=\"HMAC-SHA1\",oauth_timestamp=\"" + oauth_timestamp + "\",oauth_nonce=\"" + oauth_nonce
					+ "\",oauth_version=\"1.0\",oauth_signature=\"" + Encoder.encode(oauth_signature) + "\",oauth_token=\"" + Encoder.encode(Vars.oauth_token)
					+ "\"";
			Document doc = Jsoup.connect("https://api.twitter.com/1.1/statuses/update.json").data("oauth_consumer_key", Vars.twitter_consumer_key)
					.data("oauth_nonce", oauth_nonce).data("oauth_signature_method", oauth_signature_method).data("oauth_timestamp", oauth_timestamp)
					.data("oauth_token", Encoder.encode(Vars.oauth_token)).data("oauth_version", "1.0").data("status", mesageText)
					.header("Authorization", authorization_header_string).header("Content-Type", "application/x-www-form-urlencoded").ignoreContentType(true)
					.maxBodySize(0).timeout(600000).ignoreHttpErrors(true).post();

		} catch (Exception e) {
			Log.e("+++", e.toString());
		}

	}
}
