package com.example.name.khartn.twitter.test2.adapters;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.name.khartn.twitter.test2.R;
import com.example.name.khartn.twitter.test2.SecondActivity;
import com.example.name.khartn.twitter.test2.utils.Vars;

public class MyListViewAdapter extends BaseAdapter {

	private static LayoutInflater inflater = null;
	ArrayList<String> twits;

	public MyListViewAdapter(ArrayList<String> twits) {
		this.twits = twits;
		inflater = (LayoutInflater) Vars.act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		return twits.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View vi;

		vi = inflater.inflate(R.layout.list_view_text_element, null);
		TextView tv = (TextView) vi.findViewById(R.id.lvtext);

		String text = twits.get(position);

		SpannableString ss = new SpannableString(text);

		// Search for Users
		String patternStr = "(?:\\s|\\A)[@]+([A-Za-z0-9-_]+)";
		Pattern pattern = Pattern.compile(patternStr);
		Matcher matcher = pattern.matcher(text);
		while (matcher.find()) {
			String result = "";

			result = matcher.group();

			Integer regionStart = matcher.start();
			Integer regionEnd = matcher.end();

			if (result.startsWith(" ")) {
				regionStart++;
			}
			result = result.replace(" ", "");
			ss.setSpan(new MyClickableSpan(text.substring(regionStart, regionEnd)), regionStart, regionEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

		}

		tv.setText(ss);
		tv.setMovementMethod(LinkMovementMethod.getInstance());

		return vi;
	}

}

class MyClickableSpan extends ClickableSpan { // clickable span
	String name;

	public MyClickableSpan(String name) {
		this.name = name.replace("@", "");
	}

	public void onClick(View textView) {
		 Vars.secondUserName = name;

		Intent secondTwitsList = new Intent(Vars.act, SecondActivity.class);
		Vars.act.startActivity(secondTwitsList);

	}

	@Override
	public void updateDrawState(TextPaint ds) {
		ds.setColor(Color.BLUE);// set text color

		ds.setUnderlineText(false); // set to false to remove underline
	}
}
