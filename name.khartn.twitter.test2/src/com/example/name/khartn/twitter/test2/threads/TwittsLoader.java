package com.example.name.khartn.twitter.test2.threads;

import java.util.ArrayList;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.ListView;

import com.example.name.khartn.twitter.test2.R;
import com.example.name.khartn.twitter.test2.adapters.MyListViewAdapter;
import com.example.name.khartn.twitter.test2.utils.Vars;

public class TwittsLoader extends Thread {

	String userNameLocal;
	public TwittsLoader(String userName) {
		userNameLocal=userName;
	}

	public void run() {
		try {

			String auth = Vars.twitter_consumer_key + ":" + Vars.twitter_consumer_secret;
			ArrayList<String> twits=new ArrayList<String>();

			byte[] apacheBytes = org.apache.commons.codec.binary.Base64.encodeBase64(auth.getBytes("UTF-8"));
			String fromApacheBytes = new String(apacheBytes, "UTF-8");

			Document doc = Jsoup.connect("https://api.twitter.com/oauth2/token").header("Accept-Encoding", "gzip")
					.header("Authorization", "Basic " + fromApacheBytes).header("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8")
					.data("grant_type", "client_credentials").ignoreContentType(true).post();
			String response = doc.text();

			JSONObject obj = (JSONObject) JSONValue.parse(response);

			String accessToken = obj.get("access_token").toString();

			doc = Jsoup.connect("https://api.twitter.com/1.1/statuses/user_timeline.json")
					.data("screen_name", userNameLocal)
					.data("count", "10")
					.header("Accept-Encoding", "gzip").header("Authorization", "Bearer " + accessToken).ignoreContentType(true).maxBodySize(0).timeout(600000)
					.get();

			response = doc.text();
			JSONArray outerArray = (JSONArray) JSONValue.parse(response);
			for (Object tweet : outerArray) {
				JSONObject jSONObject = (JSONObject) tweet;
				String tweetText = jSONObject.get("text").toString();
				twits.add(tweetText);

			}

			Handler handler = new Handler(Looper.getMainLooper());
			final ArrayList<String> twitsFinal=twits;
			handler.post(new Runnable() {

				@Override
				public void run() {
					ListView lv = (ListView) Vars.act.findViewById(R.id.listView1);
					lv.setAdapter(new MyListViewAdapter(twitsFinal));
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
			Log.e("e", e.toString());
		}
	}

}
