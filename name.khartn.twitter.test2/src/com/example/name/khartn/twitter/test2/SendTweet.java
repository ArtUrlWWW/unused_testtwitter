package com.example.name.khartn.twitter.test2;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.example.name.khartn.twitter.test2.threads.TwitSender;

public class SendTweet extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_send_tweet);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.send_tweet, menu);
		return true;
	}
	
	public void sendTweet(View view) {
		TextView tv=(TextView) findViewById(R.id.editText1);
		String messText=tv.getText().toString();
		TwitSender twitSender=new TwitSender(messText);
		twitSender.start();
	}

}
